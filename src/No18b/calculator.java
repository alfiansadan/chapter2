package No18b;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class calculator {

	private JFrame frame;
	private JTextField textField;
	//private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					calculator window = new calculator();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public calculator() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 210, 253);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(0, 0, 193, 30);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("7");
		btnNewButton.setBounds(0, 29, 49, 49);
		frame.getContentPane().add(btnNewButton);
		
		JButton button = new JButton("4");
		button.setBounds(0, 76, 49, 49);
		frame.getContentPane().add(button);
		
		JButton button_1 = new JButton("9");
		button_1.setBounds(96, 29, 49, 49);
		frame.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("/");
		button_2.setBounds(144, 29, 49, 49);
		frame.getContentPane().add(button_2);
		
		JButton button_3 = new JButton("5");
		button_3.setBounds(48, 76, 49, 49);
		frame.getContentPane().add(button_3);
		
		JButton button_4 = new JButton("8");
		button_4.setBounds(48, 29, 49, 49);
		frame.getContentPane().add(button_4);
		
		JButton button_5 = new JButton("6");
		button_5.setBounds(96, 76, 49, 49);
		frame.getContentPane().add(button_5);
		
		JButton button_6 = new JButton("*");
		button_6.setBounds(144, 76, 49, 49);
		frame.getContentPane().add(button_6);
		
		JButton button_7 = new JButton("1");
		button_7.setBounds(0, 124, 49, 49);
		frame.getContentPane().add(button_7);
		
		JButton button_8 = new JButton("2");
		button_8.setBounds(48, 124, 49, 49);
		frame.getContentPane().add(button_8);
		
		JButton button_9 = new JButton("3");
		button_9.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		}
		});
		
		button_9.setBounds(96, 124, 49, 49);
		frame.getContentPane().add(button_9);
		
		JButton button_10 = new JButton("-");
		button_10.setBounds(144, 124, 49, 49);
		frame.getContentPane().add(button_10);
		
		JButton button_11 = new JButton("0");
		button_11.setBounds(0, 172, 49, 49);
		frame.getContentPane().add(button_11);
		
		JButton button_12 = new JButton(".");
		button_12.setBounds(48, 172, 49, 49);
		frame.getContentPane().add(button_12);
		
		JButton button_13 = new JButton("=");
		button_13.setBounds(96, 172, 49, 49);
		frame.getContentPane().add(button_13);
		
		JButton button_14 = new JButton("+");
		button_14.setBounds(144, 172, 49, 49);
		frame.getContentPane().add(button_14);
	}
}

