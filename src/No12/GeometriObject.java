package No12;


public class GeometriObject {
	private String color ="white";
	private boolean filled;
	private java.util.Date dateCreated;
	
	public GeometriObject() {
		dateCreated = new java.util.Date();
	}
	/**
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}
	/**
	 * @return the filled
	 */
	public boolean isFilled() {
		return filled;
	}
	/**
	 * @param filled the filled to set
	 */
	public void setFilled(boolean filled) {
		this.filled = filled;
	}
	/**
	 * @return the dateCreated
	 */
	public java.util.Date getDateCreated() {
		return dateCreated;
	}
	
	public void colorDes()
	{
		System.out.println("created on " + dateCreated + "color: "+ color+ " and filled: "+filled);
	}
}