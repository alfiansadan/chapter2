package No12;

public class Triangle extends GeometriObject {
	private double side1, side2, side3;
	
	/**
	 * @param side1
	 * @param side2
	 * @param side3
	 */
	public Triangle(double side1, double side2, double side3) {
		this.side1 = 1;
		this.side2 = 1;
		this.side3 = 1;
	}
		
	/**
	 * @return the side1
	 */
	public double getSide1() {
		return side1;
	}

	/**
	 * @param side1 the side1 to set
	 */
	public void setSide1(double side1) {
		this.side1 = side1;
	}

	/**
	 * @return the side2
	 */
	public double getSide2() {
		return side2;
	}
	/**
	 * @param side2 the side2 to set
	 */
	public void setSide2(double side2) {
		this.side2 = side2;
	}
	/**
	 * @return the side3
	 */
	public double getSide3() {
		return side3;
	}
	/**
	 * @param side3 the side3 to set
	 */
	public void setSide3(double side3) {
		this.side3 = side3;
	}
	/**
	 * 
	 */
	public Triangle() {
		this.side1 = 1;
		this.side2 = 1;
		this.side3 = 1;
	}
	public void calSemiPeri() {
		double  s	= (this.side1 + this.side2 + this.side3)/2;
	}
	public void getArea() {
		
		double  s	= (this.side1 + this.side2 + this.side3)/2;
//		Output (Area) = sqrt {s * (s - Input1) * (s - Input2) * (s - Input3)}
		double area = Math.sqrt(s*(s-this.side1)*(s-this.side2)*(s-this.side3));
		System.out.println("Total area : " +area);
	}
	
	public void getPerimeter() {
		double perimeter = this.side1 + this.side2 + this.side3;
		System.out.println("Total perimeter: " +perimeter);
	}
	
	public void desToString() {
		System.out.println("Triangle: side1 = "+ this.side1 +" side2= "+this.side2 +" side3= "+this.side3);
	}
	
}