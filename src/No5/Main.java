package No5;

public class Main {
	public static void main(String[] args){
		
		MyInteger m = new MyInteger(12);
		
		System.out.println(m.isEven());
		System.out.println(m.isOdd());
		System.out.println(m.isPrime());
		
		System.out.println(MyInteger.isEven(15));
		System.out.println(MyInteger.isOdd(15));
		System.out.println(MyInteger.isPrime(15));
	}
}
