package No11;
import java.util.ArrayList;
import java.util.Arrays;

public class Anagram {
	public static void main(String[] args) {
		 
		System.out.println(isAnagram("silent", "listen"));
		System.out.println(isAnagramByUsingArrays("garden", "ranged"));
		System.out.println(isAnagramUsingSB("split", "lisp"));
 
		System.out.println(isAnagramByUsingArrays("cabb", "abc"));
 
	}
 
	/*
	 * This method assumes both arguments are not null and in lowercase.
	 *
	 * @return true, if both String are anagram
	 */
	public static boolean isAnagram(String str1, String str2) {
		if (str1.length() != str2.length()) {
			return false;
		}
		
		char[] chars = str1.toCharArray();
 
		for (char c : chars) {
			int index = str2.indexOf(c);
			if (index != -1) {
				str2 = str2.substring(0, index) + str2.substring(index + 1, str2.length());
			} else {
				return false;
			}
		}
 
		return str2.isEmpty();
	}
 
	/**
	 * In this method help StringBuilder to check whether two string are anagram
	 * or not.
	 * 
	 * @param first
	 * @param second
	 * @return
	 */
	public static boolean isAnagramUsingSB(String first, String second) {
		char[] characters = first.toCharArray();
		StringBuilder sbSecond = new StringBuilder(second);
 
		for (char ch : characters) {
			int index = sbSecond.indexOf("" + ch);
			if (index != -1) {
				sbSecond.deleteCharAt(index);
			} else {
				return false;
			}
		}
 
		return sbSecond.length() == 0 ? true : false;
	}
 
	/*
	 * This java method to check two string are anagram or not by using library.
	 * input of both should be in either lower case only or upper case only.
	 * 
	 * @return true, if both Strings are anagram.
	 */
	public static boolean isAnagramByUsingArrays(String str1, String str2) {
 
		char[] charFromStr1 = str1.toCharArray();
		char[] charFromStr2 = str2.toCharArray();
		Arrays.sort(charFromStr1);
		Arrays.sort(charFromStr2);
 
		return Arrays.equals(charFromStr1, charFromStr2);
	}	
}
