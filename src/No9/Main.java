package No9;


public class Main {
	
	public static void main(String[] args) {
        int [] counts = count("23333AB3");
        for (int i = 0; i < counts.length; i++) {
            System.out.println("counts of [" + i + "] is: " + counts[i]);
        }
    }
	
	public static int[] count(String s){
        int [] counts = new int[10];
        char [] c = s.toCharArray();
        int counter = 0;
        for(int i=0;i<counts.length;i++){
            for(int j=0;j<c.length;j++){
                    if(Character.isDigit(c[j]) && i == Character.getNumericValue(c[j])){
                        counter++;
                }
            }
            counts[i] = counter;
            counter = 0;
        }
        return counts;
    }
}
